<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('register');
    }
    public function welcome(Request $request){
        // View all submitted data:
        // dd($request->all());

        // Contain data to variables. There are two ways:

        // oop syntax
        $firstname = $request->input('firstname');

        // Associative array syntax
        $lastname = $request['lastname'];

        // We will use associative array one for brevity for now
        $gender = $request['gender'];
        $nationality = $request['nationality'];
        $lang = $request['lang'];
        $bio = $request['bio'];

        return view('welcome', [
            'firstname' => $firstname,
            'lastname' => $lastname,
            'gender' => $gender,
            'nationality' => $nationality,
            'lang' => $lang,
            'bio' => $bio
        ]);
    }
}
