@extends('layout.master')

@section('title')
    Show Cast {{$casts->id}}
@endsection

@section('subtitle')
    Data diri {{$casts->nama}}
@endsection

@section('content')
<p><strong>Umur:</strong> {{$casts->umur}} tahun</p>
<p><strong>Biodata:</strong><br>{{$casts->bio}}</p>
@endsection