@extends('layout.master')

@section('title')
    Post Editor
@endsection

@section('subtitle')
    Edit Post {{$casts->id}}
@endsection

@section('content')
    <form action="/cast/{{$casts->id}}" method="POST">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label for="bana">Nama</label>
            <input type="text" class="form-control" name="nama" value="{{$casts->nama}}" id="title" placeholder="Masukkan nama cast">
            @error('nama')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="umur">Usia</label>
            <input type="text" class="form-control" name="umur" value="{{$casts->umur}}"  id="body" placeholder="Masukkan umur">
            @error('umur')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="bio">Bio</label>
            <textarea cols="60" rows="10" class="form-control" name="bio" id="bio" placeholder="Masukkan biodata">{{$casts->bio}}</textarea>
            @error('bio')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Edit</button>
    </form>
@endsection