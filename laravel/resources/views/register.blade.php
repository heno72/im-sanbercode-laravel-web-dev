@extends('layout.master')

@section('title')
    Buat Account Baru!
@endsection

@section('subtitle')
    Sign Up Form
@endsection

@section('content')
    <form action="/welcome" method="POST">
        @csrf
        <label>Nama Depan:</label><br><br>
        <input type="text" name="firstname"><br><br>
        <label>Nama Belakang:</label><br><br>
        <input type="text" name="lastname"><br><br>
        <label>Jenis Kelamin:</label><br><br>
        <input type="radio" name="gender" value="1" id="gender">Male <br>
        <input type="radio" name="gender" value="2" id="gender">Female <br>
        <input type="radio" name="gender" value="0" id="gender">Other <br>
        <br>
        <label>Nasionalitas:</label><br><br>
        <select name="nationality" id="nationality">
            <option value="id">Indonesia</option>
            <option value="usa">United States of America</option>
            <option value="th">Thailand</option>
            <option value="ko">South Korea</option>
            <option value="my">Malaysia</option>
            <option value="sg">Singapore</option>
        </select><br><br>
        <label>Bahasa Utama:</label><br><br>
        <select name="lang" id="nationality">
            <option value="id_ID">Indonesia</option>
            <option value="en_US">English</option>
            <option value="other">Other</option>
        </select>
        <br>
        <br>
        Bio:
        <br>
        <textarea name="bio" id="bio" cols="30" rows="10"></textarea><br><br>
        <input type="submit" value="Sign Up">
    </form>
@endsection