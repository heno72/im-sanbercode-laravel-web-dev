@extends('layout.master')

@section('title')
    SanberBook
@endsection

@section('subtitle')
    Social Media Developer Santai Berkualitas
@endsection

@section('content')
    <p>
        Belajar dan Berbagi agar hidup ini semakin santai berkualitas.
    </p>
    <h3>
        Benefit Join di SanberBook
    </h3>
    <div>
        <ul>
            <li>Mendapatkan motivasi dari sesama developer</li>
            <li>Sharing knowledge dari para mastah Sanber</li>
            <li>Dibuat oleh calon web developer terbaik</li>
        </ul>
    </div>
    <h3>
        Cara bergabung ke SanberBook
    </h3>
    <div>
        <ol>
            <li>Mengunjungi Website ini</li>
            <li>Mendaftar di <a href="{{ url('/register') }}">Form Sign Up</a></li>
            <li>Selesai!</li>
        </ol>
    </div>
@endsection
