@extends('layout.master')

@section('title')
    Selamat Datang {{ $firstname }} {{ $lastname }}!
@endsection

@section('subtitle')
    Terima kasih telah bergabung di Sanberbook. Social Media kita bersama!
@endsection

@section('content')
    <p>
        Jenis Kelamin:
        @switch($gender)
            @case(1)
                Laki-laki
                @break
            @case(2)
                Perempuan
                @break
            @default
                Nonbinary
        @endswitch
        <br>
        Nasionalitas:
        @switch($nationality)
            @case('id')
                Indonesia
                @break
            @case('usa')
                Amerika Serikat
                @break
            @case('th')
                Thailand
                @break
            @case('ko')
                South Korea
                @break
            @case('my')
                Malaysia
                @break
            @case('sg')
                Singapura
                @break
            @default
                <em>(tidak diketahui)</em>
        @endswitch
        <br>
        Bahasa utama:
        @switch($lang)
            @case('id_ID')
                Bahasa Indonesia
                @break
            @case('en_US')
                Bahasa Inggris
                @break
            @default
                Lainnya
        @endswitch
        <br>
        Bio: {{ $bio }}
    </p>
@endsection
