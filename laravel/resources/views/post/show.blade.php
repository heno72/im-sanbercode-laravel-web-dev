@extends('layout.master')

@section('title')
    Show Post {{$post->id}}
@endsection

@section('subtitle')
    {{$post->title}}
@endsection

@section('content')
<p>{{$post->body}}</p>
@endsection