<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\CastController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'home']);

Route::get('/register', [AuthController::class, 'register']);

Route::post('/welcome', [AuthController::class, 'welcome']);

Route::get('/table',function (){
    return view('partial.table');
});

Route::get('/data-tables',function (){
    return view('partial.data-tables');
});

// Route for casts
Route::get('/cast', [CastController::class, 'index']);
Route::get('/cast/create', [CastController::class, 'create']);
Route::post('/cast', [CastController::class, 'store']);
Route::get('/cast/{cast_id}', [CastController::class, 'show']);
Route::get('/cast/{cast_id}/edit', [CastController::class, 'edit']);
Route::put('/cast/{cast_id}', [CastController::class, 'update']);
Route::delete('/cast/{cast_id}', [CastController::class, 'destroy']);

// Testing
// Route::get('/master',function (){
//     return view('layout.master');
// });

// Practice area
Route::get('/posts', [PostController::class, 'index']);
Route::get('/posts/create', [PostController::class, 'create']);
Route::post('/post', [PostController::class, 'store']);
Route::get('/posts/{post_id}', [PostController::class, 'show']);
Route::get('/posts/{post_id}/edit', [PostController::class, 'edit']);
Route::put('/posts/{post_id}', [PostController::class, 'update']);
Route::delete('/posts/{post_id}', [PostController::class, 'destroy']);

// Redirection
Route::get('/post', function (){
    return redirect('/posts');
});