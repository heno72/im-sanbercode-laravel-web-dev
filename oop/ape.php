<?php
class Ape extends Animal {
    public $legs = 2;

    public function yell() {
        echo "Auoooo<br>";
    }

    public function intro() {
        echo "
            Name: $this->name<br>
            Legs: $this->legs<br>
            Cold blooded: $this->cold_blooded<br>
            Yell: ";
        $this->yell();
        echo "<br>";
    }
}