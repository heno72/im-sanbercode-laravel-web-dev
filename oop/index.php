<?php

// Class definitions
require_once('animal.php');
require_once('ape.php');
require_once('frog.php');

// Invocations below

$sheep = new Animal("shaun");

echo "<h4>Display contents of the class with var_dump()</h4>";
var_dump($sheep);

// echo "<h4>Display contents of the class with echo</h4>";
// echo "$sheep->name<br>"; // "shaun"
// echo "$sheep->legs<br>"; // 4
// echo "$sheep->cold_blooded<br>"; // "no"

// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())
echo "<h4>Method get (get_name(), get_legs(), get_cold_blooded())</h4>";
$sheep->get_name(); // "shaun"
$sheep->get_legs(); // 4
$sheep->get_cold_blooded(); // "no"

echo "<h4>Create new class Ape, extended from class Animal with extra method yell()</h4>";
$sungokong = new Ape("kera sakti");
$sungokong->yell(); // "Auooo"

echo "<h4>Create new class Frog, extended from class Animal with extra method jump()</h4>";
$kodok = new Frog("buduk");
$kodok->jump() ; // "hop hop"

echo "<h4>Output akhir:</h4>";
echo "<div style=\"padding: 10px; margin: 10px\">";
$sheep->intro();
$sungokong->intro();
$kodok->intro();
echo "</div>";
