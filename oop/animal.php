<?php
class Animal {
    public $name, $legs = 4, $cold_blooded = 'no';

    public function __construct($name) {
        $this->name = $name;
    }

    public function get_name() {
        echo $this->name . '<br>';
    }

    public function get_legs() {
        echo $this->legs . '<br>';
    }

    public function get_cold_blooded() {
        echo $this->cold_blooded . '<br>';
    }

    public function intro() {
        echo "
            Name: $this->name<br>
            Legs: $this->legs<br>
            Cold blooded: $this->cold_blooded<br>
            <br>
            ";
    }

}
