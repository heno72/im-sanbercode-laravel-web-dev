<?php
class Frog extends Animal {
    public $cold_blooded = 'yes';

    public function jump() {
        echo "Hop Hop<br>";
    }

    public function intro() {
        echo "
            Name: $this->name<br>
            Legs: $this->legs<br>
            Cold blooded: $this->cold_blooded<br>
            Jump: ";
        $this->jump();
        echo "<br>";
    }
}