
<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<title>Function</title>
</head>

<body>
<h1>Berlatih Function PHP</h1>
<?php

echo "<h3> Soal No 1 Greetings </h3>";
/* 
Soal No 1
Greetings
Buatlah sebuah function greetings() yang menerima satu parameter berupa string. 

contoh: greetings("abduh");
Output: "Halo Abduh, Selamat Datang di Sanbercode!"
*/

// Code function di sini

// I assumes that 'nama peserta' is meant to be using my name
$nama_peserta = "Hendrik Lie";

function greetings($name = 'User') {
    echo "Halo $name, Selamat Datang di Sanbercode!<br>";
}

// Hapus komentar untuk menjalankan code!
greetings("Bagas");
greetings("Wahyu");
// greetings("nama peserta");
greetings($nama_peserta);

echo "<br>";

echo "<h3>Soal No 2 Reverse String</h3>";
/* 
Soal No 2
Reverse String
Buatlah sebuah function reverseString() untuk mengubah string berikut menjadi kebalikannya menggunakan function dan looping (for/while/do while).
Function reverseString menerima satu parameter berupa string.
NB: DILARANG menggunakan built-in function PHP sepert strrev(), HANYA gunakan LOOPING!

reverseString("abdul");
Output: ludba

*/

// Code function di sini 
function reverseString($target, $shout = true ) {
    $chars = str_split($target);
    for ( $i = 0 ; $i < strlen($target) ; $i++ ) {
        $output .= $chars[strlen($target) - 1 - $i] ;
    }
    // The following is added so we can configure the function
    // to only return values without echo, but echoes in normal
    // circumstances
    if ( $shout === true ) {
        echo $output . '<br>';
    }
    return $output;
}
// $target = 'Hello World';
// print_r(str_split($target));

// Hapus komentar di bawah ini untuk jalankan Code
// reverseString("nama peserta");
reverseString($nama_peserta);
reverseString("Sanbercode");
reverseString("We Are Sanbers Developers");

echo "<br>";

echo "<h3>Soal No 3 Palindrome </h3>";
/* 
Soal No 3 
Palindrome
Buatlah sebuah function yang menerima parameter berupa string yang mengecek apakah string tersebut sebuah palindrome atau bukan. 
Palindrome adalah sebuah kata atau kalimat yang jika dibalik akan memberikan kata yang sama contohnya: katak, civic.
Jika string tersebut palindrome maka akan mengembalikan nilai true, sedangkan jika bukan palindrome akan mengembalikan false.
NB: 
Contoh: 
palindrome("katak") => output : "true"
palindrome("jambu") => output : "false"
NB: DILARANG menggunakan built-in function PHP seperti strrev() dll. Gunakan looping seperti biasa atau gunakan function reverseString dari jawaban no.2!

*/


// Code function di sini

function palindrome($check, $shout = true) {
    // Store the reversed string in a variable
    $reversed_string = reverseString($check,false);
    // Checks if the string is a string and a palindrome
    // Gives boolean output
    if ( $check === $reversed_string ) {
        $isPalindrome = true;
    } else {
        $isPalindrome = false;
    }

    // The following is added so we can configure the function
    // to only return values without echo, but echoes in normal
    // circumstances
    if ( $shout === true ) {
        // The variable $isPalindrome contains a boolean value
        // We wanted a human readable string
        if ( $isPalindrome === true ) {
            echo "true<br>";
        } else {
            echo "false<br>";
        }
    }

    return $isPalindrome;
}

// Hapus komentar di bawah ini untuk jalankan code
palindrome("civic") ; // true
palindrome("nababan") ; // true
palindrome("jambaban"); // false
palindrome("racecar"); // true


echo "<h3>Soal No 4 Tentukan Nilai </h3>";
/*
Soal 4
buatlah sebuah function bernama tentukan_nilai . Di dalam function tentukan_nilai yang menerima parameter 
berupa integer. dengan ketentuan jika paramater integer lebih besar dari sama dengan 85 dan lebih kecil sama dengan 100 maka akan mereturn String “Sangat Baik” 
Selain itu jika parameter integer lebih besar sama dengan 70 dan lebih kecil dari 85 maka akan mereturn string “Baik” selain itu jika parameter number lebih besar 
sama dengan 60 dan lebih kecil dari 70 maka akan mereturn string “Cukup” selain itu maka akan mereturn string “Kurang”
*/

/*
    Logic (in bash notation):
    # Logic is modified to reject values below zero and above one hundred
    if [[ $input -ge 0 && $input -lt 60 ]]; then
        echo "Kurang"
    elif [[ $input -ge 60 && $input -lt 70 ]]; then
        echo "Cukup"
    elif [[ $input -ge 70 && $input -lt 85 ]]; then
        echo "Baik"
    elif [[ $input -ge 85 && $input -le 100 ]]; then
        echo "Sangat Baik"
    else # for cases of $input below 0 or above 100
        echo "Bukan range nilai yang valid (harus integer antara 0-100)"
    fi
*/

// Code function di sini
// This function is called with echo, so it needs to just return its results
function tentukan_nilai($nilai) {
    if ( $nilai >= 0 && $nilai < 60 ) {
        $nisbi = "Kurang";
    } else if ( $nilai >= 60 && $nilai < 70 ) {
        $nisbi = "Cukup";
    } else if ( $nilai >= 70 && $nilai < 85 ) {
        $nisbi = "Baik";
    } else if ( $nilai >= 85 && $nilai <= 100 ) {
        $nisbi = "Sangat Baik";
    } else {
        $nisbi = "Bukan range nilai yang valid (harus integer antara 0-100)";
    }
    // Adds a newline
    $nisbi .='<br>';
    return $nisbi;
}

// Hapus komentar di bawah ini untuk jalankan code
echo tentukan_nilai(98); //Sangat Baik
echo tentukan_nilai(76); //Baik
echo tentukan_nilai(67); //Cukup
echo tentukan_nilai(43); //Kurang

?>

</body>

</html>
