<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Array</title>
</head>

<body>
    <h1>Berlatih Array</h1>

    <?php
    echo "<h3> Soal 1 </h3>";
    /* 
            SOAL NO 1
            Kelompokkan nama-nama di bawah ini ke dalam Array.
            Kids : "Mike", "Dustin", "Will", "Lucas", "Max", "Eleven" 
            Adults: "Hopper", "Nancy",  "Joyce", "Jonathan", "Murray"
        */
        $kids = [ "Mike", "Dustin", "Will", "Lucas", "Max", "Eleven" ];
        $adults = [ "Hopper", "Nancy",  "Joyce", "Jonathan", "Murray" ];
        $kids_count = count($kids);
        $adults_count = count($adults);

        // For debug purposes
        // $array_kids=print_r($kids,true);
        // echo "<pre>$array_kids</pre>";
        // $array_adults=print_r($adults,true);
        // echo "<pre>$array_adults</pre>";

        // For debug purposes, so I know that I added elements to the array properly
        // echo "Kids : ";
        // for($k = 0 ; $k < $kids_count ; $k++) {
        //     echo "\"$kids[$k]\"";
        //     if ( $k < $kids_count-1 ) {
        //         echo ", ";
        //     } else {
        //         echo "<br>";
        //     }
        // }
        // echo "Adults : ";
        // for($a = 0 ; $a < $adults_count ; $a++) {
        //     echo "\"$adults[$a]\"";
        //     if ( $a < $adults_count-1 ) {
        //         echo ", ";
        //     } else {
        //         echo "<br>";
        //     }
        // }

    echo "<h3> Soal 2</h3>";
    /* 
            SOAL NO 2
            Tunjukkan panjang Array di Soal No 1 dan tampilkan isi dari masing-masing Array.
        */
    echo "Cast Stranger Things: ";
    echo "<br>";
    echo "Total Kids: $kids_count"; // Berapa panjang array kids
    echo "<br>";
    echo "<ol>";
    echo "<li> $kids[0] </li>";
    // Lanjutkan
    echo "<li> $kids[1] </li>";
    echo "<li> $kids[2] </li>";
    echo "<li> $kids[3] </li>";
    echo "<li> $kids[4] </li>";
    echo "<li> $kids[5] </li>";

    // Within the constraint of the problem, I must not use for loop yet.
    // However below is the code to do it with for loop
    // for($k = 0 ; $k < $kids_count ; $k++) {
    //     echo "<li> $kids[$k] </li>";
    // }

    echo "</ol>";

    echo "Total Adults: $adults_count"; // Berapa panjang array adults
    echo "<br>";
    echo "<ol>";
    echo "<li> $adults[0] </li>";
    // Lanjutkan
    echo "<li> $adults[1] </li>";
    echo "<li> $adults[2] </li>";
    echo "<li> $adults[3] </li>";
    echo "<li> $adults[4] </li>";

    // Within the constraint of the problem, I must not use for loop yet.
    // However below is the code to do it with for loop
    // for($a = 0 ; $a < $adults_count ; $a++) {
    //     echo "<li> $adults[$a] </li>";
    // }

    echo "</ol>";

    /*
            SOAL No 3
            Susun data-data berikut ke dalam bentuk Asosiatif Array didalam Array Multidimensi
            
            Name: "Will Byers"
            Age: 12,
            Aliases: "Will the Wise"
            Status: "Alive"

            Name: "Mike Wheeler"
            Age: 12,
            Aliases: "Dungeon Master"
            Status: "Alive"

            Name: "Jim Hopper"
            Age: 43,
            Aliases: "Chief Hopper"
            Status: "Deceased"

            Name: "Eleven"
            Age: 12,
            Aliases: "El"
            Status: "Alive"


            Output:
            Array
                (
                    [0] => Array
                        (
                            [Name] => Will Byers
                            [Age] => 12
                            [Aliases] => Will the Wise
                            [Status] => Alive
                        )

                    [1] => Array
                        (
                            [Name] => Mike Wheeler
                            [Age] => 12
                            [Aliases] => Dugeon Master
                            [Status] => Alive
                        )

                    [2] => Array
                        (
                            [Name] => Jim Hooper
                            [Age] => 43
                            [Aliases] => Chief Hopper
                            [Status] => Deceased
                        )

                    [3] => Array
                        (
                            [Name] => Eleven
                            [Age] => 12
                            [Aliases] => El
                            [Status] => Alive
                        )

                )
            
        */
    $people = [
        [
            "Name" => "Will Byers",
            "Age" => 12,
            "Aliases" => "Will the Wise",
            "Status" => "Alive"
        ],
        [
            "Name" => "Mike Wheeler",
            "Age" => 12,
            "Aliases" => "Dungeon Master",
            "Status" => "Alive"
        ],
        [
            "Name" => "Jim Hopper",
            "Age" => 43,
            "Aliases" => "Chief Hopper",
            "Status" => "Deceased"
        ],
        [
            "Name" => "Eleven",
            "Age" => 12,
            "Aliases" => "El",
            "Status" => "Alive"
        ]
    ];

    // For debug purposes
    // $content_people=print_r($people,true);
    // echo "<h3>Soal 3</h3><pre>$content_people</pre>";
    ?>
</body>

</html>